//
//  ContactsModel.swift
//  Contacts App week 3
//
//  Created by Tom Holmes @ TAE on 06/03/2020.
//  Copyright © 2020 Tom Holmes @ TAE. All rights reserved.
//

import Foundation

struct Contact {
    var name: String
    var moblieNumber: String?
}


class ContactsModel {
    private var contactIndex: [Character: [Contact]]
    init() {
        contactIndex = [:]
    }
}


extension Contact: Equatable {
    static func == (lhs: Contact, rhs: Contact) -> Bool {
        return
            lhs.name == rhs.name &&
                lhs.moblieNumber == rhs.moblieNumber
    }
}


extension ContactsModel {
    var sections: [Character] { return contactIndex.keys.sorted() }
    
    func firstLetter(_ contactNameFirstLetter: Character) -> Bool {
        return contactIndex.keys.contains(contactNameFirstLetter)
    }
    
    func numberOfRows(in section: Int) -> Int {
        guard section < sections.count else { return 0 }
        
        let key = sections[section]
        
        guard let values = contactIndex[key] else { return 0 }
        
        return values.count
    }
    
    
    func contact(at indexPath: IndexPath) -> Contact? {
        if indexPath.section < 0 || indexPath.section > sections.count { return nil }
        
        let key = sections[indexPath.section]
        
        guard let values = contactIndex[key], indexPath.row < values.count else { return nil }
        
        return values[indexPath.row]
    }
    
    
    func addContactRow(_ contact: Contact) {
        guard let key = contact.name.first else {
            return
        }
        
        for ContactSection in contactIndex.values {
            if ContactSection.contains(contact) {
                return
            }
        }
        
        if firstLetter(key) {
            contactIndex[key]?.append(contact)
            contactIndex[key]?.sort(by: {$0.name < $1.name})
        } else {
            let newContactSection = [contact]
            contactIndex[key] = newContactSection
        }
    }
    
    
    func addContact(at indexPath: IndexPath) -> Contact? {
        guard indexPath.section < sections.count else { return nil }
        
        let key = sections[indexPath.section]
        
        guard let getSection = contactIndex[key], indexPath.row < getSection.count else { return nil }
        
        return getSection[indexPath.row]
    }
}


