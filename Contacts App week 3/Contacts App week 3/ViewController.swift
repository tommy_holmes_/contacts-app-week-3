//
//  ViewController.swift
//  Contacts App week 3
//
//  Created by Tom Holmes @ TAE on 06/03/2020.
//  Copyright © 2020 Tom Holmes @ TAE. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    enum PhoningError: Error {
        case noNumber
        case noName
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var numberTextField: UITextField!
    
    var model: ContactsModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        model = ContactsModel()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    @IBAction func addContactPressed(_ sender: UIButton) {
        if nameTextField.text?.isEmpty ?? true {
            
            let alert = UIAlertController(title: "No name entered", message: "Please enter a name", preferredStyle: .alert)
            
            let okay = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okay)
            
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            guard let fullName = nameTextField.text, fullName.count > 0 else { return }
            
            let number = numberTextField.text ?? ""
            
            let newContact = Contact(name: fullName, moblieNumber: number)
            
            model.addContactRow(newContact)
            
            tableView.reloadData()
            nameTextField.text = ""
            numberTextField.text = ""
        }
        
    }
    
    
    func callActionSheet(_ indexPath: IndexPath) throws {
        let callMe = model.addContact(at: indexPath)
        guard let contactName = callMe?.name,
            let contactNumber = callMe?.moblieNumber,
            !contactNumber.isEmpty
            else {
                throw PhoningError.noNumber
        }
        
        let alertController = UIAlertController(title: "Call Them, Maybe", message: "Hey, \(contactName) just met you, and they are crazy, here’s their number \(contactNumber) so call them maybe?", preferredStyle: .actionSheet)
        
        let phone = UIAlertAction(title: "Via Phone", style: .default, handler: nil)
        
        let skype = UIAlertAction(title: "Via Skype", style: .default, handler: nil)
        
        let whatsApp = UIAlertAction(title: "Via WhatsApp", style: .default, handler: nil)
        
        let cancel = UIAlertAction(title: "How about no", style: .destructive, handler: nil)
        
        alertController.addAction(phone)
        alertController.addAction(skype)
        alertController.addAction(whatsApp)
        alertController.addAction(cancel)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func noNumberAction() {
        let alertController = UIAlertController(title: "No phone number", message: "Can not call", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(ok)
        
        self.present(alertController, animated: true, completion: nil)
    }
}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return model.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactsTableCell", for: indexPath)
        
        guard let currentContact = model.addContact(at: indexPath) else {
            return cell
        }
        
        cell.textLabel?.text = currentContact.name
        cell.detailTextLabel?.text = currentContact.moblieNumber
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return String(model.sections[section])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        do {
            try callActionSheet(indexPath)
        } catch (PhoningError.noNumber) {
            noNumberAction()
        } catch {
            print(error)
        }
    }
    
}


